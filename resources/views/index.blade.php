@extends('app')

@section('content')
    <div class="row">
        <div class="col-md-push-2 col-md-8">
            <h1>Butthurthim's skins</h1>
            @foreach($skins as $skin)
                <div>
                    <h3>{{ $skin->title }} - <a href="{{ $skin->link }}">Download</a></h3>
                    <p>
                        <div class="row">
                            <div class="col-md-4">
                                <p>
                                    <a href="{{ $skin->screenshot1 }}" data-lightbox="image-{{ $skin->id }}" data-title="{{ $skin->title }}">
                                        <img src="{{ $skin->screenshot1 }}" class="img-responsive" />
                                    </a>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <p>
                                    <a href="{{ $skin->screenshot2 }}" data-lightbox="image-{{ $skin->id }}" data-title="{{ $skin->title }}">
                                        <img src="{{ $skin->screenshot2 }}" class="img-responsive" />
                                    </a>
                                </p>
                            </div>
                            <div class="col-md-4">
                                <p>
                                    <a href="{{ $skin->screenshot3 }}" data-lightbox="image-{{ $skin->id }}" data-title="{{ $skin->title }}">
                                        <img src="{{ $skin->screenshot3 }}" class="img-responsive" />
                                    </a>
                                </p>
                            </div>
                        </div>
                    </p>
                </div>
            @endforeach
        </div>
    </div>
@stop
