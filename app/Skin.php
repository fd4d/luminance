<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skin extends Model {

    protected $table = 'skins';

    protected $fillable = [
        'title',
        'screenshot1',
        'screenshot2',
        'screenshot3',
        'link'
    ];
}
