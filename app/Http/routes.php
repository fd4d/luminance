<?php

$app->get('/', function () use ($app) {
    $skins = app('db')->select('SELECT * FROM skins');
    return view('index', compact('skins'));
});
