<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Skin;

class SkinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skin::create([
            'title' => 'EVB smile skin Luftwaffe remake',
            'screenshot1' => '/screenshots/11.png',
            'screenshot2' => '/screenshots/12.png',
            'screenshot3' => '/screenshots/13.png',
            'link' => '/skins/1.osk'
        ]);
        Skin::create([
            'title' => 'psuh-ncuh',
            'screenshot1' => '/screenshots/21.png',
            'screenshot2' => '/screenshots/22.png',
            'screenshot3' => '/screenshots/23.png',
            'link' => '/skins/2.osk'
        ]);
        Skin::create([
            'title' => 'Another EVB remake by Luftwaffe',
            'screenshot1' => '/screenshots/31.png',
            'screenshot2' => '/screenshots/32.png',
            'screenshot3' => '/screenshots/33.png',
            'link' => '/skins/3.osk'
        ]);
        Skin::create([
            'title' => 'stayfrosty + new circles',
            'screenshot1' => '/screenshots/41.png',
            'screenshot2' => '/screenshots/42.png',
            'screenshot3' => '/screenshots/43.png',
            'link' => '/skins/4.osk'
        ]);
    }
}
