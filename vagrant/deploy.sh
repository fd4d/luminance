#!/bin/bash

basedir="/home/vagrant/luminance"

cd $basedir

apt-get install -y curl software-properties-common python-software-properties
add-apt-repository ppa:ondrej/php-7.0
curl -sL https://deb.nodesource.com/setup_5.x | bash -
apt-get install -y vim git php7.0-fpm php7.0-cli php7.0-sqlite3 nginx #nodejs

usermod -G vagrant www-data
usermod -G www-data vagrant

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

composer install

#npm install -g gulp

nginx="
server {
    listen 80;

    root $basedir/public;
    index index.php;

    server_name luminance.dev;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
        sendfile off;
    }

    location ~ \.php$ {
        try_files \$uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $basedir/public\$fastcgi_script_name;
        include fastcgi_params;
    }
}
"

echo $nginx > /etc/nginx/sites-available/luminance
ln -s /etc/nginx/sites-available/luminance /etc/nginx/sites-enabled/luminance
rm /etc/nginx/sites-enabled/default

service nginx restart

touch $basedir/database/database.sqlite

cp .env.example .env
php artisan migrate
